package com.chatbot.pizza.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chatbot.pizza.model.PizzaOrder;



public interface PizzaRepo extends JpaRepository<PizzaOrder, Long> {

	PizzaOrder save (PizzaOrder pizzaorder);
	
	PizzaOrder findByOrderId(String orderId);
}
