package com.chatbot.pizza.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.pizza.model.PizzaOrder;
import com.chatbot.pizza.repository.PizzaRepo;

@Service
public class UserService {

	@Autowired PizzaRepo pizzaRep;
	
	 public PizzaOrder save(PizzaOrder pizzaOrder) {
		 return pizzaRep.save(pizzaOrder);
	 }
	 
	 public PizzaOrder findOne(String orderId) {
		 
		 return pizzaRep.findByOrderId(orderId);
	 }
}
