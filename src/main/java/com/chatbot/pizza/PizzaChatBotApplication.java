package com.chatbot.pizza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaChatBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaChatBotApplication.class, args);
	}

}
