$(document).ready(function(){
   
	// button Disable From Starting
	$('.sendButton').prop('disabled', true);
	
	//create search class
    $(".chat_on").click(function(){
        $(".Layout").toggle();
        $(".chat_on").hide(300);
        
        
    });
    
       $(".chat_close_icon").click(function(){
        $(".Layout").hide();
           $(".chat_on").show(300);
    });
       
     
    
})



var me = {};
me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";

var you = {};
you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time){
    if (time === undefined){
        time = 0;
    }
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        control = '<li style="width:100%">' +
                        '<div class="msj macro">' +
                        
                            '<div class="text text-l">' +
                                '<label style="color:black">'+ text +'</label>' +
                                
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else if(who == "content"){
    	control = '<li style="width:100%">' +
        '<div class=" macro">' +
        
        '<div class="text text-l">' +
            '<label style="color:black">'+ text +'</label>' +
            
        '</div>' +
    '</div>' +
'</li>'; 
    }
    
    else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<label style="color:black">'+text+'</label>' +
                                
                            '</div>' 
                                                       
                  '</li>';
    }
    setTimeout(
        function(){                        
            $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
            $("#vegPizza").on("click",orderClick);
            $(".nonVeg").on("click",nonVeg);
            $(".pizzaName").on("click",pizzaClick);
            $(".size").on("click",sizeClick);
            $(".name").on("click",Name);
            $(".newClass").on("keypress",emailFunction);
            
            
           
            
            //For nonVeg
            function nonVeg(){
            	 var pizza = $(this).text();
            	 insertChat("you", pizza);
            	 
            	 insertChat("content", "<div><h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Pepper Barbecue Chicken</h4>&nbsp;<h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Chicken Sausage</h4>"+
            			    "&nbsp;<h4 id='pizzaName' class='badge badge-pill badge-primary pizzaName'>Chicken Golden Delight</h4><h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Non Veg Supreme</h4>"
            			    +"</div>",0);
            }
        
    //For Order Veg
    function orderClick(){
    	var pizza = $("#vegPizza").text();
	    insertChat("you", pizza);
	    
	    insertChat("content", "<div><h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Peppy Paneer</h4>&nbsp;<h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Mexican Green Wave</h4>"+
	    "&nbsp;<h4 id='pizzaName' class='badge badge-pill badge-primary pizzaName'>Margherita</h4><h4 class='badge badge-pill badge-primary pizzaName' id='pizzaName'>Double Cheese Margherita</h4>"
	    +"</div>",0);
	    
	  }
    //For PizzaName
    function pizzaClick(){
    	 var pizza = $(this).text();
    	 insertChat("you", pizza);
    	 
    	 insertChat("content", "<div style='display:flex;justify-content:center'><h4 class='badge badge-pill badge-primary size' >Regular</h4>&nbsp;<h4 class='badge badge-pill badge-primary size' id='pizzaName'>Medium</h4>"+
    			    "&nbsp;<h4 id='pizzaName' class='badge badge-pill badge-primary size'>Large</h4>"
    			    +"</div>",0);
    }
    
    //size of pizza
    function sizeClick(){
    	 var pizza = $(this).text();
    	 insertChat("you", pizza);
    	 
    	 insertChat("me", "How many",0);
    	 insertChat("content", "<div style='display:flex;justify-content:center'><h4 class='badge badge-pill badge-primary name ' >1</h4>&nbsp;<h4 class='badge badge-pill badge-primary name' >2</h4>&nbsp;<h4 class='badge badge-pill badge-primary name' >3</h4>&nbsp;<h4 class='badge badge-pill badge-primary name' >4</h4>&nbsp;<h4 class='badge badge-pill badge-primary name' >5</h4></div>",0);
    }
    //Taking Name
    function Name(){
    	var pizza = $(this).text();
   	 	insertChat("you", pizza);
   	 	$('.sendButton').prop('disabled', false);
   	 	insertChat("me", "Got it. Let me just take your info 😊",0);
   		insertChat("me", "First up, what's your name?",0);
   		
    }  
  }) 
}






//For EMAIL
    function emailFunction(){
	 var keycode = (event.keyCode ? event.keyCode : event.which);
     if(keycode == '13'){
    	 insertChat("me", " great",0); 
     }
	event.stopPropagation();
    	
    }


function resetChat(){
    $("ul").empty();
}



$('body > div > div > div:nth-child(2) > span').click(function(){
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
})


//-- Clear Chat
//resetChat();

//-- Print Messages
insertChat("me", "Hello")
insertChat("me", "What You want ?")
insertChat("content", "<div style='display:flex;justify-content:center'><h4 class='badge badge-pill badge-primary order' id='orderPizza'>order pizza</h4>&nbsp;<h4 class='badge badge-pill badge-primary track' id='trackId'>Track order</h4></div>", 0);



//-- NOTE: No use time on insertChat.


  $(document).ready(function(){
	  
//	  Hit For getting email
	  $("#sendEmail").on("click",function(){
			var text = $("#myText").val();
			
			 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			 if(!regex.test(text)) {
				 alert("Please Enter a valid Email Address")
				    return false;
				  }
			   
			var Id = Math.floor(1000 + Math.random() * 9000);
			var orderId = "PI"+Id;
			
		    if (text !== ""){
		        insertChat("you", text); 
		        insertChat("me", "Please Provide You Mobile Number",0)

		   	 	$(".sendButtonEmail").hide();
		   		 $(".sendNumber").show();
		   		$("#myText").val('');
		    }

		})
		//For Sending Number
		$("#sendNum").on("click",function(){
			var text = $("#myText").val();
			
			 phone = text.replace(/[^0-9]/g,'');
		        if (phone.length != 10)
		        {
		            alert('Phone number must be 10 digits.');    
		            return false;
		        }
			 
			        
	
			if(text == ""){
				return;
			}
			var Id = Math.floor(1000 + Math.random() * 9000);
			var orderId = "PI"+Id;
			
			if (text !== ""){
		        insertChat("you", text); 
		        
		        insertChat("me", "Awesome. Your order is placed",0);
		  		 insertChat("me", "Your Order Id is : "+orderId ,0);
		  	    insertChat("me", "You'll soon get a call for confirmation 👋",0); 
		   	 	$(".sendNumber").hide();
		   		 $(".sendButton").show();
		   		$("#myText").val('');
		    }
			$('.sendButton').prop('disabled', true);
			
			var data = {
		    		"orderId":orderId,
		    		"status":"preparing"
		    }
			//ajax
		    $.ajax({
		    	 type: "POST",
			       contentType : 'application/json; charset=utf-8',
			       dataType : 'json',
			       url: "/pizzaOrder",
			       data: JSON.stringify(data), // Note it is important
			       success :function(result) {
			     	console.log("Order SuccessFully and Your Order Id is : " + result.orderId);
			       },
			       error:function(err){
			    	   console.log("error :" + err);
			       }
		    })
		})
		
		// for the first order
	  $(".order").on("click",function(){
		    $(".trackBtn").hide();
		    $(".sendButton").show();
		    $('.sendButton').prop('disabled', true);
		    var pizza = $("#orderPizza").text();
		    insertChat("you", pizza);
		    
		    insertChat("content", "<div style='display:flex;justify-content:center'><h4 class='badge badge-pill badge-primary vegOrder' id='vegPizza' >Veg</h4>&nbsp;<h4 class='badge badge-pill badge-warning nonVeg'>Non Veg</h4></div>",0);
		    
		   
	  });
	  
	  $(".track").on("click",function(){
		  $(".sendButton").hide();
		  $(".trackBtn").show();
		  var pizza = $("#trackId").text();
		    insertChat("you", pizza);
		    insertChat("me","Enter Your Order Id",0);
		    $('.sendButton').prop('disabled', false);
		    
	  })
	  
	  $("#send").on("click",function(){
	
	var text = $("#myText").val();
	
    if (text !== ""){
        insertChat("you", text); 
        insertChat("me", " what's your Email Id",0);
        
   	 	$(".sendButton").hide();
   		 $(".sendButtonEmail").show();
   		$("#myText").val('');
    }
	
})
	  $("#trackBt").on("click",function(){
		  
		  var text = $("#myText").val();
		  if(text == ""){
			  return;
		  }
		  if (text !== ""){
		        insertChat("you", text); 
		        
		         $(".sendButton").show();
				  $(".trackBtn").hide();
				  $('.sendButton').prop('disabled', true);
		   		//$("#myText").val('');
		    }
		  
		  $.ajax({
		    	 type: "Get",
			       contentType : 'application/json; charset=utf-8',
			       dataType : 'json',
			       url: "/track/?orderId="+text,
			        
			       success :function(result) {
			    	  
			     	if(result == null){
			     		return false;
			     	}else{
			     		insertChat("me", result.status,0);
			     	}
			     	
			       },
			       error:function(err){
			    	   console.log("error :" + err);
			    	   alert("Order Id is not found !")
			       }
		    })
		  
		  
	  })
	  
  })
  
 
