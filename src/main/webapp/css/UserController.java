package com.chatbot.pizza.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chatbot.pizza.model.PizzaOrder;
import com.chatbot.pizza.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	UserService service;

	  @PostMapping(path = "/pizzaOrder")
	    public @ResponseBody PizzaOrder order(@RequestBody PizzaOrder pizzaOrder) {
	    	System.out.print(pizzaOrder);
			PizzaOrder pizzaSuccess =  service.save(pizzaOrder);
			return pizzaSuccess;
	    	
	    }
	
	

}
